﻿Car myCar = new Car();
myCar.Brand = "Toyota";
myCar.Model = "Camry";
myCar.Year = 2020;
var wheelsNumber = myCar.WheelsNumber;

myCar.StartEngine();    
myCar.StopEngine();
string newRedColor = "Red";
myCar.ChangeColor(newRedColor, true);
Console.WriteLine(myCar.GetDescription());

Car anotherCar = new Car();
anotherCar.Brand = "Honda";
anotherCar.Model = "Accord";
anotherCar.Year = 2018;
var doorsNumber = myCar.DoorsNumber;

anotherCar.StartEngine();    
anotherCar.StopEngine();
string newBlueColor = "Blue";
anotherCar.ChangeColor(isMetallic: false, newColor: newBlueColor);
Console.WriteLine(anotherCar.GetDescription());

public class Car
{
    // Fields (data)
    private string brand;
    private string model;
    private int year;
    private int wheelsNumber = 4;
    private int doorsNumber = 5;
    private string color;

    // Properties
    public string Brand
    {
        get { return brand; }
        set { brand = value; }
    }

    public string Model
    {
        get => model;
        set => model = value;
    }

    public int Year
    {
        get { return year; }
        set
        {
            if (value >= 1886) // The year the first car was invented
                year = value;
            else
                throw new Exception("Invalid year for a car.");
        }
    }

    public int Сolor { get; set; }

    public int WheelsNumber
    {
        get { return wheelsNumber; }
    }

    public int DoorsNumber => doorsNumber;
 
    // Methods (behavior)				
    public void StartEngine()
    {
        Console.WriteLine("Engine started.");
    }

    public void StopEngine()
    {
        Console.WriteLine("Engine stopped.");
    }

    public void ChangeColor(string newColor)
    {
        color = newColor;
    }

    public void ChangeColor(string newColor, bool isMetallic)
    {
        color = isMetallic ? newColor + " Metallic" : newColor;
    }

    public string GetDescription()
    {
        return $"Brand: {brand}, Model: {model}, Year: {year}, Color: {color}";
    }
}