# Object-Oriented Programming C Sharp

**Estimated reading time**: 60 min

## Story Outline
The story provides detailed insights into how to efficiently use OOP in C#. 
It also includes designing and implementing classes, understanding the principles of encapsulation, inheritance, polymorphism and abstraction. 
The course is designed for those who do not have a basic understanding of C# and are interested in enhancing their knowledge of how to use OOP in C#.

## Story Organization
**Story Branch**: main
> `git checkout main`


Tags: #OOP, #.NET, #C_Sharp #Encapsulation #Polymorphism #Inheritance #Abstraction